const {ObjectId} = require('mongodb'); // or ObjectID
const ApiController = require('./ApiController');
const DateFormatter = require("../lib/Modules/DateFormatter");
const date = require('moment');

const ProjectModel = require("../models/ProjectModel").Project;
const BidderModel = require("../models/BidderModel").Bidder;
const ChatProjectModel = require("../models/ChatProjectModel").ChatProject;

const ChatProjectFinder = require("../ProjectMarketPlace/Repository/Finder/ChatProjectFinder");
const ChatProject = require("../ProjectMarketPlace/Repository/ChatProject");

class ChatProjectController extends ApiController
{
    async getProjectModel(id)
    {
        let row = await ProjectModel.findOne({_id: id}).populate({
            path: 'bid_info',
            model: BidderModel
        }).sort({created_at: -1}).exec();

        if(!row)
            throw new Error('Project tidak ditemukan');

        return row;
    }

    async getModel(projectId)
    {
        let row = await ChatProjectModel.findOne({project_id: projectId}).sort({created_at: -1}).exec();

        if(!row)
            throw new Error('Anda belum pernah Chat');

        return row;
    }

    controller()
    {
        return {
            index: async (req,res) => 
            {
                let finder = new ChatProjectFinder(req.user_info.user_type);
                this.jsonResponse = this.jResp();
                try {
                    if(req.query.per_page)
                        finder.setPerPage(req.query.per_page);

                    if(req.query.page)
                        finder.setPage(req.query.page);
                    
                    if(req.query.keyword)
                        finder.setKeyword(req.query.keyword);
                    
                    if(req.query.order_by) {
                        finder.orderBy(req.query.order_by['column'], req.query.order_by['ordered']);
                    } else {
                        finder.orderBy('created_at', -1);
                    }
                    
                    if(req.params.id)
                        finder.setProject(req.params.id);
                    
                    var paginator = await finder.get();
                    
                    let list = [];
                    paginator.data.forEach(x => {
                        var fromDetail = x.from_detail.length ? x.from_detail[0].fullname : '';
                        var toDetail = x.to_detail.length ? x.to_detail[0].fullname : '';
                        var data = {
                            project_id: x.project_id,
                            from: fromDetail,
                            to: toDetail,
                            message: x.message,
                            created_at: x.created_at
                        }

                        list.push(data);
                    })
                    
                    this.jsonResponse.setData(list);
                    this.jsonResponse.setMeta(this.jsonResponse.getPaginatorConfig(paginator));
                } catch(e) {
                    
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse());
            },
            sendMessageWorker: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new ChatProjectModel;
                    
                    // Get Id
                    let id = req.params.id;
                    
                    let row = await this.getProjectModel(id);

                    let repo = new ChatProject(model);

                    model.project_id = ObjectId(id);
                    model.from_id = ObjectId(req.user.user_id);
                    model.to_id = ObjectId(row.user_id);
                    model.message = req.body.message;
                    model.created_at = now;
                    
                    await repo.save();
                    
                    this.jsonResponse.setMessage(`Anda berhasil mengirim pesan.`);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            },
            sendMessageUser: async (req,res) => 
            {
                const dateFormatter = new DateFormatter();
                const now = dateFormatter.date(date);
                this.jsonResponse = this.jResp();
                try {
                    let model = new ChatProjectModel;
                    
                    // Get Id
                    let id = req.params.id;
                    
                    await this.getProjectModel(id);

                    let row = await this.getModel(id)

                    let repo = new ChatProject(model);
                    
                    model.project_id = ObjectId(id);
                    model.from_id = ObjectId(req.user.user_id);
                    model.to_id = ObjectId(row.from_id);
                    model.message = req.body.message;
                    model.created_at = now;
                    
                    await repo.save();
                    
                    this.jsonResponse.setMessage(`Anda berhasil mengirim pesan.`);

                } catch(e) {
                    this.jsonResponse.setMessage(e.message);
                    this.jsonResponse.setError(true);
                }

                return res.status(200).send(this.jsonResponse.getResponse())
            }
        }
    }
}

let chatProject = new ChatProjectController();
module.exports.ChatProjectController = chatProject.controller();