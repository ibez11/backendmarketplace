const AbstractRepository = require("../Repository/AbstractRepository");
const Validator = require('node-input-validator');


const ChatProjectModel = require("../../models/ChatProjectModel").ChatProject;

class ChatProject extends AbstractRepository {
    constructor(model = new ChatProjectModel) {
        super(model);
    }

    async save()
    {
        let validator = null;
        
        Validator.addCustomMessages({
            'message.required': 'Message kosong',
        });

        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });

        // Validation
        let fields = {
            message: this.model.message
        };

        let rules = {
            message: 'required'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);

        this.model.save();
    }
}

module.exports = ChatProject;