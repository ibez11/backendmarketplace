const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');
const {ObjectId} = require('mongodb'); // or ObjectID
const serviceAccount = require("../../key/key.json");

const PasswordGenerate = require("../../ThebadusLibs/BadusPasswordGenerate");

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const AbstractRepository = require("../Repository/AbstractRepository");
const UserModel = require("../../models/UserModel").User;
const UserLoggedModel = require("../../models/UserLoggedModel").userLogged;

const Validator = require('node-input-validator');

class User extends AbstractRepository {
    constructor(model = new UserModel) {
        super(model);
    }

    async validate() 
    {
        let validator = null;
        
        Validator.extend('in', async function ({ value }) {
            if( value == true )
                return true;
            
            return false;
        });
        
        Validator.addCustomMessages({
            'username.required': 'Username harus diisi.',
            'password.required': 'Password harus diisi',
            'matched.in': 'Username atau Password salah',
            'is_actived.in': 'User tidak aktif lagi'
        });

        var detailUser = await this.getDetailUser(this.model.username)
        var check = false;
        var is_actived = false;
        
        if(detailUser) {
            var pw = detailUser.password;
            is_actived = detailUser.is_actived;
            check = bcrypt.compareSync(this.model.password, pw);
        }
        
        var fields = {
            username: this.model.username,
            matched: check,
            is_actived: is_actived,
            password: this.model.password,
        };
        
        var rules = {
            username:'required',
            password: 'required',
            is_actived: 'in:true,false',
            matched: 'in:true,false',
        }

        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);
    }

    async signIn(ip) {
        var result = [];

        await this.validate()
        
        var detailUser = await this.getDetailUser(this.model.username);
        
        const payload = {
            check: true,
            user_id: detailUser._id,
            ip: ip
        };

        const token = jwt.sign(payload, serviceAccount.private_key, { algorithm: 'RS256', header: {
            kid: serviceAccount.private_key_id
        }, expiresIn: '356d'});
        
        let setParamData = {
            jwt: token,
            user_id: detailUser._id,
            username: detailUser.username
        }
        
        await this.setUserLogged(setParamData);

        result['token'] = token;
        result['message'] = 'Anda berhasil login';

        return result;
    }

    async getDetailUser(username) 
    {
        var x = new Promise( function (fulfilled, rejected) {
            UserModel.findOne({username: username}, function(err, data) {
                if(err) rejected(err)
                
                if(data) {
                    let opts = [];
                    
                    UserModel.populate(data, opts, function(err, data) {
                        if(err)
                        rejected(err)
                        
                        fulfilled(data);
                    })
                } else {
                    fulfilled(null);
                }
            })
        });

        return x;
    }

    async getDetailUserById(user_id) 
    {
        var x = new Promise( async function (fulfilled, rejected) {
            await UserModel.findOne({_id: ObjectId(user_id), is_actived: true}, function(err, data) {
                if(err) rejected(err)
                
                if(data) {
                    let opts = [];
                    
                    UserModel.populate(data, opts, function(err, data) {
                        fulfilled(data);
                    })
                } else {
                    fulfilled([]);
                }
            })
        });

        return x;
    }

    async setUserLogged(param) 
    {
        var dateFormatter = new DateFormatter();
        var now = dateFormatter.date(date);

        await UserLoggedModel.deleteMany({ user_id: param.user_id }, function(err) {
            if (err) {
                console.log(err);
            }
        });
        var userLoggedModel = new UserLoggedModel;
        // userLoggedModel._id = ObjectId(param.user_id);
        userLoggedModel.user_id = param.user_id;
        userLoggedModel.username = param.username;
        userLoggedModel.role_type = param.role_type;
        userLoggedModel.jwt_encrypt = param.jwt;
        userLoggedModel.created_at = now;
        
        userLoggedModel.save({_id: ObjectId(param.user_id)}, (err, doc) => {
            if(err)
            console.log(err)
        });
    }

    async getUserLogged(param) 
    {
        let isLogged = await new Promise(async function (fulfilled, rejected) {
            let result = null;
            let row = await UserLoggedModel.findOne({user_id: ObjectId(param.user_id)}).populate('user').exec();
            
            if(row && row.user.length) {
                result = row;
            }
            
            fulfilled(result);
        });
       
        return isLogged;
    }

    async save()
    {
        let validator = null;
        
        var checkUsername = await UserModel.countDocuments({username: this.model.username}).exec();
        
        Validator.addCustomMessages({
            'password_length.minLength': 'Password minimum 6',
            'password_length.required': 'Password harus diisi',
            'username.required': 'Username harus diisi',
            'username_already_exist.in': 'Username sudah terdaftar',
            'fullname.required': 'Fullname harus diisi'
        });
        
        Validator.extend('in', async function (field, value) {
            if( field.value == true )
              return true;
            
            return false;
        });

        // Validation
        let fields = {
            password_length: this.model.password,
            username: this.model.username,
            username_already_exist: checkUsername == 0,
            fullname: this.model.fullname
        };
        
        let rules = {
            password_length: 'required|minLength:5',
            username: 'required',
            username_already_exist: 'in:true,false',
            fullname: 'required'
        };
        
        validator = new Validator.Validator( fields, rules );
        await this.validOrThrow(validator);

        this.model.password = PasswordGenerate(this.model.password);
        this.model.save();
    }
}

module.exports = User;