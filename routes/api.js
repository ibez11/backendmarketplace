var express = require("express");
var router = express.Router();

const { ProjectController } = require('../controllers/ProjectController');
const { WorkerController } = require('../controllers/WorkerController');
const { CategoryController } = require('../controllers/CategoryController');
const { ChatProjectController } = require('../controllers/ChatProjectController');

const { auth } = require('../controllers/AuthController');
const { Auth } = require('../middleware/Auth');

// Category
router.get('/master/category/list', CategoryController.index);
router.get('/master/category/detail/:id', CategoryController.show);

// Auth User
router.post('/user/login', auth.signInUser);
router.post('/user/register', auth.registerUser);

// Auth Worker
router.post('/worker/login', auth.signInWorker);
router.post('/worker/register', auth.registerWorker);

// Worker List - User
router.get('/master/user/worker-list', Auth.middleware(['user']).isAuth, WorkerController.index);
router.get('/master/user/worker-detail/:id', Auth.middleware(['user']).isAuth, WorkerController.show);
router.post('/master/user/worker-create', Auth.middleware(['user']).isAuth, WorkerController.store);
router.post('/master/user/worker-set-isactived/:id', Auth.middleware(['user']).isAuth, WorkerController.setStatusIsActived);

// Project User
router.get('/master/user/project/list', Auth.middleware(['user']).isAuth, ProjectController.index);
router.get('/master/user/project/detail/:id', Auth.middleware(['user']).isAuth, ProjectController.show);
router.post('/master/user/project/create', Auth.middleware(['user']).isAuth, ProjectController.store);

// Worker Project Comment
router.post('/master/user/project/comment-project-worker/:id', Auth.middleware(['user']).isAuth, ProjectController.commentProjectWorker);

// Project Worker
router.get('/master/worker/project/bid-list', Auth.middleware(['worker']).isAuth, ProjectController.bidWorker);
router.get('/master/worker/project/list', Auth.middleware(['worker']).isAuth, ProjectController.index);
router.get('/master/worker/project/detail/:id', Auth.middleware(['worker']).isAuth, ProjectController.show);
router.post('/master/worker/project/bid/:id', Auth.middleware(['worker']).isAuth, ProjectController.setBid);
router.post('/master/worker/project/finish/:id', Auth.middleware(['worker']).isAuth, ProjectController.finish);

// Chat
router.get('/master/worker/project-message-list/:id', Auth.middleware(['worker']).isAuth, ChatProjectController.index);
router.get('/master/user/project-message-list/:id', Auth.middleware(['user']).isAuth, ChatProjectController.index);

router.post('/master/worker/project-send-message/:id', Auth.middleware(['worker']).isAuth, ChatProjectController.sendMessageWorker);
router.post('/master/user/project-send-message/:id', Auth.middleware(['user']).isAuth, ChatProjectController.sendMessageUser);


// Canceled Project After One Hours
router.post('/master/worker/project/canceled-after-one-hours/:id', Auth.middleware(['worker']).isAuth, ProjectController.setCancelAfteOneHours);

router.get("/", function(req,res) {
    res.status(500).send({
        success: false,
        message: "Maaf data tidak tersedia"
    })
});

module.exports = router;