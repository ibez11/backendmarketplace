var express = require('express');
var fileupload = require("express-fileupload");
var cookieParser = require('cookie-parser');
var cors = require('cors');
var api = require('./routes/api');
var bodyParser = require('body-parser');

var app = express();
const port = process.env.APP_PORT;

app.options('*', cors())

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}))
app.use(fileupload());

app.use(cookieParser());

// API
app.use('/api/', api);

app.get('*', (req, res) => 
    res.status(500).send({
        success: false,
        message: "Uppss!!!"
    })
);

app.listen(port);
console.log("Server started on port " + port);

module.exports = app;