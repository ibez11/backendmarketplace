var { Seeder } = require('mongoose-data-seed');
var faker = require('faker');
const {ObjectId} = require('mongodb'); // or ObjectID
const Model = require("../../models/UserModel").User;
const DateFormatter = require("../../lib/Modules/DateFormatter");
const date = require('moment');

let data = [];

class UsersSeeder extends Seeder {

    async shouldRun() {
        return Model.deleteMany({}).exec();
    }
  
    async run() {
        const dateFormatter = new DateFormatter();
        const now = dateFormatter.date(date);

        data.push({
            _id: ObjectId(),
            fullname: 'Joko Susilo',
            no_hp: '082383324024',
            username: 'joko',
            password: "$2b$10$NWn5T79me4pLm0bJddaPLuOwIxWek35nV0qdOMDfE2b2MdGbYaP.O",
            user: 'user',
            created_at: now
        });

        return Model.create(data);
    }
}

module.exports = UsersSeeder;