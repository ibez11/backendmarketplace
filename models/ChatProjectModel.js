'use strict';
var {mongoDB} = require("./Db");
var table = 'chatProjects';

var Schema = mongoDB.Schema;
var ChatProjectSchema = new Schema({
    project_id: {type: Schema.Types.ObjectId, default: null},
    from_id: {type: Schema.Types.ObjectId, default: null},
    to_id: {type: Schema.Types.ObjectId, default: null},
    message: {type: String, default: ''},
    created_at: {type: String, default: ''}
});

var ChatProject = mongoDB[process.env.DB_NAME].model(table, ChatProjectSchema, table);
module.exports.ChatProject = ChatProject;