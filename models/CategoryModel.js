'use strict';
var {mongoDB} = require("./Db");
var table = 'categories';

var Schema = mongoDB.Schema;
var CategorySchema = new Schema({
    _id: {type: Number},
    name: {type: String, default: ''},
    label: {type: String, default: ''},
    notes: {type: String, default: ''},
    group_by: {type: String, default: ''},
    created_at: {type: String, default: ''},
    updated_at: {type: String, default: ''}
});

var Category = mongoDB[process.env.DB_NAME].model(table, CategorySchema, table);
module.exports.Category = Category;