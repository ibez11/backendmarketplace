'use strict';
var {mongoDB} = require("./Db");
var table = 'projects';

class ProjectStatuses {
    constructor(){
        this.STATUS_PROGRESS = 28;
        this.STATUS_CANCELED = 29;
        this.STATUS_WAITING = 30;
        this.STATUS_FINISH = 31;

        this.PAYMENT_METHOD_CASH = 0;
        this.PAYMENT_METHOD_WALLET = 1;
    }

    getStatusIdByName(name)
    {
        let statusId = null;

        switch(name) {
            case 'progress':
                statusId = this.STATUS_PROGRESS;
                break;

            case 'canceled':
                statusId = this.STATUS_CANCELED;
                break;

            case 'waiting':
                statusId = this.STATUS_WAITING;
                break;

            case 'finish':
                statusId = this.STATUS_FINISH;
                break;
        }

        return statusId;
    }
}

let projectStatuses = new ProjectStatuses;
module.exports.ProjectStatuses = projectStatuses;

var Schema = mongoDB.Schema;
var ProjectSchema = new Schema({
    title: {type: String, default: ''},
    description: {type: String, default: ''},
    category_id: {type: Number, default: null},
    user_id: {type: Schema.Types.ObjectId, default: null},
    status: {type: Number, default: 30},
    budget: {type: Number, default: 0},
    number_of_worker: {type: Number, default: 0},
    is_specific_budget: {type: Boolean, default: false},
    transaction_no: {type: String, default: ''},
    payment_method: {type: Number, default: 0},
    created_at: {type: String, default: ''},
    updated_at: {type: String, default: ''}
});

ProjectSchema.virtual('bid_info',{
    ref: 'bidders',
    localField: '_id',
    foreignField: 'project_id',
    default: {},
    justOne: true
});

ProjectSchema.virtual('category_info',{
    ref: 'categories',
    localField: 'category_id',
    foreignField: '_id',
    default: {},
    justOne: true
});

ProjectSchema.set('toObject', { virtuals: true });
ProjectSchema.set('toJSON', { virtuals: true });

var Project = mongoDB[process.env.DB_NAME].model(table, ProjectSchema, table);
module.exports.Project = Project;