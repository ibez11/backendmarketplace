'use strict';
const table = 'projectCommentWorkers';
const {mongoDB} = require('./Db')

mongoDB.set('useCreateIndex', true);
var Schema = mongoDB.Schema;
var CurrentSchema = new Schema({
    user_id: {type: Schema.Types.ObjectId, default: null},
    worker_id: {type: Schema.Types.ObjectId, default: null},
    project_id: {type: Schema.Types.ObjectId, default: null},
    rating: {type: Number, default: 5},
    comment: {type: String, default: ''}
});

var CurrentModel = mongoDB[process.env.DB_NAME].model(table, CurrentSchema, table);
module.exports.ProjectCommentWorker = CurrentModel;